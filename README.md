# Refugee Finder by @iDoMeteor


Feel free to hit up the issues or wiki, and I most certainly welcome
commentary via @iDoMeteor on Twitter or on http://forums.meteor.com.

There is also a Trello board for team communication and task prioritization
here: https://trello.com/b/Lutuif4Z/refugee-finder

Contributions in the form of pull requests are also welcome!
