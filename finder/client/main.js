Template.alertProfile.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #proximity': function() {
    RF.popModal('proximity');
  },

  'click #parameters': function() {
    RF.popModal('parameters');
  },

  'click #image-match': function() {
    RF.popModal('imageMatch');
  },

});



Template.alertProfileNew.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #proximity': function() {
    RF.popModal('proximity');
  },

  'click #parameters': function() {
    RF.popModal('parameters');
  },

  'click #threshold': function() {
    // This will be a slider, not a button
  },

});



Template.alerts.events({

  'click #new-alert': function() {
    FlowRouter.go('alertProfileNew');
  },

  'click .rf-alert': function() {
    FlowRouter.go('alertProfile');
  },

});



Template.associationNew.events({

  'click #name': function() {
    RF.popModal('name');
  },

});



Template.associations.events({

  'click #new-association': function() {
    FlowRouter.go('associationNew');
  },

  'click .rf-association': function() {
    FlowRouter.go('association');
  },

});



Template.autoMatch.events({

  'click #new-match-profile': function() {
    FlowRouter.go('matchProfileNew');
  },

  'click .rf-match-profile': function() {
    FlowRouter.go('matchProfile');
  },

});



Template.browse.events({

  'click #names': function() {
    FlowRouter.go('browseNames');
  },

  'click #locations': function() {
    FlowRouter.go('browseLocations');
  },

  'click #nearby': function() {
    FlowRouter.go('browseNearby');
  },

  'click #parameters': function() {
    FlowRouter.go('browseParameters');
  },

  'click #photos': function() {
    FlowRouter.go('browsePhotos');
  },

});



Template.contact.events({

  'click #send': function() {
    RF.sendContact();
  },

});



Template.footer.events({

  'click #donate': function() {
    RF.popModal('donate');
  },

});



Template.header.events({

  'click #home': function() {
    FlowRouter.go('main');
  },

  'click #back': function() {
    window.history.back();
  },

  'click #language-selector-open': function() {
    RF.popModal('languageSelector');
  },

  'click #contact-open': function() {
    RF.popModal('contact');
  },

});



Template.languageSelector.events({

  'click #language-select': function() {
    RF.popModal('languageSelect');
  },

});



Template.lost.events({

  'click #set-location': function() {
    FlowRouter.go('lostSetLocation');
  },

  'click #looking': function() {
    FlowRouter.go('lostLooking');
  },

  'click #where': function() {
    FlowRouter.go('/maps/helpcenters');
  },

  'click #who': function() {
    FlowRouter.go('lostWho');
  },

});



Template.lostProfile.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #photo': function() {
    RF.popModal('photo');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #from': function() {
    RF.popModal('locationSelect');
  },

  'click #been': function() {
    RF.popModal('locationSelect');
  },

  'click #parameters': function() {
    RF.popModal('parameters');
  },

});



Template.main.events({

  'click #lost': function() {
    FlowRouter.go('lost');
  },

  'click #searching': function() {
    FlowRouter.go('searching');
  },

  'click #alerts': function() {
    FlowRouter.go('alerts');
  },

  'click #associations': function() {
    FlowRouter.go('associations');
  },

  'click #maps': function() {
    FlowRouter.go('maps');
  },

  'click #messages': function() {
    FlowRouter.go('messages');
  },


});



Template.map.events({

  'click #home': function() {
    FlowRouter.go('main');
  },

});



Template.maps.events({

  'click #help-centers': function() {
    FlowRouter.go('mapHelpCenters');
  },

  'click #border-crossings': function() {
    FlowRouter.go('mapBorderCrossings');
  },

  'click #proximity': function() {
    FlowRouter.go('mapProximity');
  },

  'click #density': function() {
    FlowRouter.go('mapDensity');
  },

  'click #chronologic': function() {
    FlowRouter.go('mapChronologic');
  },

  'click #routes': function() {
    FlowRouter.go('mapRoutes');
  },

  'click #popular': function() {
    FlowRouter.go('mapPopular');
  },

});



Template.matchProfile.events({

  'click #first-name': function() {
    RF.popModal('name');
  },

  'click #family-name': function() {
    RF.popModal('name');
  },

  'click .rf-location': function() {
    RF.popModal('locationSelect');
  },

  'click #parameters': function() {
    RF.popModal('parameters');
  },

  'click #save': function() {
    RF.matchProfileSave();
  },

});



Template.search.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #proximity': function() {
    // This will be a slider
  },

  'click #description': function() {
    RF.popModal('parameters');
  },

  'click #photo': function() {
    RF.popModal('photo');
  },

  'click #browse': function() {
    FlowRouter.go('browse');
  },

});



Template.searching.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #help-centers': function() {
    FlowRouter.go('/maps/helpcenters');
  },

  'click #who': function() {
    FlowRouter.go('searchingWho');
  },

});



Template.searchingProfile.events({

  'click #name': function() {
    RF.popModal('name');
  },

  'click #location': function() {
    RF.popModal('locationSelect');
  },

  'click #help-centers': function() {
    FlowRouter.go('searchProfileHelpCenters');
  },

  'click #who': function() {
    FlowRouter.go('searchProfileWho');
  },

});



Template.settings.events({

  'click #quiet-hours': function() {
    FlowRouter.go('settingHours');
  },

  'click #help-center': function() {
    FlowRouter.go('settingHelpCenter');
  },

  'click #border-crossing': function() {
    FlowRouter.go('settingBorderCrossing');
  },

  'click #refugee-proximity': function() {
    FlowRouter.go('settingProximityAlert');
  },

});
