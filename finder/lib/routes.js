// This might work for default route, lol
FlowRouter.route('/', {
  name: 'main',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'main'}
                      );
  }
});

// These should work, except maybe the ones w/:_id
FlowRouter.route('/alerts', {
  name: 'alerts',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'alerts'}
                      );
  }
});

FlowRouter.route('/alerts/new', {
  name: 'alertProfileNew',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'alertProfileNew'}
                      );
  }
});

FlowRouter.route('/alert/:_id', {
  name: 'alertProfile',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'alertProfile'}
                      );
  }
});

FlowRouter.route('/associations', {
  name: 'associations',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'associations'}
                      );
  }
});

FlowRouter.route('/association/new', {
  name: 'associationNew',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'associationNew'}
                      );
  }
});

FlowRouter.route('/association/:_id', {
  name: 'association',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'assocation'}
                      );
  }
});

FlowRouter.route('/match/new', {
  name: 'matchProfileNew',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'matchProfileNew'}
                      );
  }
});

FlowRouter.route('/match/:_id', {
  name: 'matchProfile',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'matchProfile'}
                      );
  }
});

FlowRouter.route('/browse/names', {
  name: 'browseNames',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'browseNames'}
                      );
  }
});

FlowRouter.route('/browse/locations', {
  name: 'browseLocations',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'browseLocations'}
                      );
  }
});

FlowRouter.route('/browse/nearby', {
  name: 'browseNearby',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'browseNearby'}
                      );
  }
});

FlowRouter.route('/browse/parameters', {
  name: 'browseParameters',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'browseParameters'}
                      );
  }
});

FlowRouter.route('/browse/photos', {
  name: 'browsePhotos',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'browsePhotos'}
                      );
  }
});

FlowRouter.route('/lost', {
  name: 'lost',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'lost'}
                      );
  }
});

FlowRouter.route('/lost/locations/current', {
  name: 'lostSetLocation',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'lostSetLocation'}
                      );
  }
});

FlowRouter.route('/lost/looking', {
  name: 'lostLooking',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'lostLooking'}
                      );
  }
});

FlowRouter.route('/lost/locations/previous', {
  name: 'lostPrevious',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'lostPrevious'}
                      );
  }
});

FlowRouter.route('/lost/lookingfor', {
  name: 'lostWho',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'lostWho'}
                      );
  }
});

FlowRouter.route('/maps', {
  name: 'maps',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'maps'}
                      );
  }
});

FlowRouter.route('/maps/helpcenters', {
  name: 'mapHelpCenters',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapHelpCenters'}
                      );
  }
});

FlowRouter.route('/maps/bordercrossings', {
  name: 'mapBorderCrossings',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapBorderCrossings'}
                      );
  }
});

FlowRouter.route('/maps/proximity', {
  name: 'mapProximity',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapProximity'}
                      );
  }
});

FlowRouter.route('/maps/density', {
  name: 'mapDensity',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapDensity'}
                      );
  }
});

FlowRouter.route('/maps/chronologic', {
  name: 'mapChronologic',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapChronologic'}
                      );
  }
});

FlowRouter.route('/maps/routes', {
  name: 'mapRoutes',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapRoutes'}
                      );
  }
});

FlowRouter.route('/maps/popular', {
  name: 'mapPopular',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'mapPopular'}
                      );
  }
});

FlowRouter.route('/searching', {
  name: 'searching',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'searching'}
                      );
  }
});

FlowRouter.route('/searching/helpcenters', {
  name: 'searchingHelpCenters',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'searchingHelpCenters'}
                      );
  }
});

FlowRouter.route('/searching/who', {
  name: 'searchingWho',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'searchingWho'}
                      );
  }
});

FlowRouter.route('/searching/profile/helpcenters', {
  name: 'searchingProfileHelpCenters',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'searchingProfileHelpCenters'}
                      );
  }
});

FlowRouter.route('/searching/profile/who', {
  name: 'searchProfileWho',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'searchProfileWho'}
                      );
  }
});

FlowRouter.route('/settings/hours', {
  name: 'settingHours',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'settingsHours'}
                      );
  }
});

FlowRouter.route('/settings/helpcenter', {
  name: 'settingHelpCenter',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'settingHelpCenter'}
                      );
  }
});

FlowRouter.route('/settings/bordercrossing', {
  name: 'settingBorderCrossing',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'settingBorderCrossing'}
                      );
  }
});

FlowRouter.route('/settings/proximityalert', {
  name: 'settingProximityAlert',
  action: function () {
    BlazeLayout.render('wrapper',
                       {main: 'settingProximityAlert'}
                      );
  }
});
