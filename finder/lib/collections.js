/*******************************************************************************
 * Collections
 * ****************************************************************************/

Crossings       = new Mongo.Collection('Crossings');
HelpCenters     = new Mongo.Collection('HelpCenters');
Locations       = new Mongo.Collection('Locations');
Log             = new Mongo.Collection('Log');
MatchProfiles   = new Mongo.Collection('MatchProfiles');
Singletons      = new Mongo.Collection('Singletons'); // Run-time configuration



/*******************************************************************************
 * Schemas
 *
 * Currently unattached and will remain that while for a while.  Provides a
 * good outline of the data structure, we will attach after we settle things
 * down a bit and are pretty comfortable we have everything figured out.
 * ****************************************************************************/

Crossings.schema = new SimpleSchema({

  name: {type: String},
  address: {type: String},
  countryOrigin: {type: String},
  countryDestination: {type: String},
  coordinates: {type: String},
  status: {type: String},
  image: {type: String},
  commentCount: {type: String},
  rating: {type: String},

});

HelpCenters.schema = new SimpleSchema({

  name: {type: String},
  address: {type: String},
  countryOrigin: {type: String},
  countryDestination: {type: String},
  coordinates: {type: String},
  status: {type: String},
  image: {type: String},
  commentCount: {type: String},
  rating: {type: String},

});

Locations.schema = new SimpleSchema({

  userId: {type: String},
  coordinates: {type: String},

});

Log.schema = new SimpleSchema({

  userId: {type: String},
  type: {type: String},
  message: {type: String},
  sent: {type: String},
  data: {type: String},

});

MatchProfiles.schema = new SimpleSchema({

  nameFirst: {type: String},
  nameLast: {type: String},
  name2First: {type: String},
  name2Last: {type: String},
  locations: {type: String},
  parameters: {type: String},
  thresholdLocation: {type: String},
  thresholdParameters: {type: String},

});

// The following can probably be attached to the user objects

/*
Alerts          = new Mongo.Collection('Alerts');
Associations = new Mongo.Collection('Associations');
Parameters = new Mongo.Collection('Parameters');
PeopleLost      = new Mongo.Collection('PeopleLost');
PeopleSearching = new Mongo.Collection('PeopleSearching');
Photos = new Mongo.Collection('Photos');
Settings = new Mongo.Collection('Settings');

Alerts.schema = new SimpleSchema({

  XXX: {type: String},

});

Associations.schema = new SimpleSchema({

  XXX: {type: String},

});

Parameters.schema = new SimpleSchema({

  XXX: {type: String},

});

PeopleLost.schema = new SimpleSchema({

  XXX: {type: String},

});

PeopleSearching.schema = new SimpleSchema({

  XXX: {type: String},

});

Photos.schema = new SimpleSchema({

  XXX: {type: String},

});


Settings.schema = new SimpleSchema({

  XXX: {type: String},

});
*/
