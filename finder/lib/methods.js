/******************************************************************************
 *
 * RF RPC Methods
 *
 * TODO:
 *      This is just stolen from RF and as yet unmodified
 *      Change to using SimpleSchema / Collections2
 *        validation or jQuery Validation
 *
 *****************************************************************************/

Meteor.methods({

  /**************************************************************************
   *
   * @Summary         Check for duplicate contact request
   * @Method          contactExists
   * @Param           n/a
   * @Returns         {boolean}   Returns true upon failure as well
   * @Location        Client, Server
   *
   * @Description
   *
   *      Checks for an existing contact request by a user's email or Twitter
   *      handle, as well as their IP.
   *
   * ************************************************************************/

  rfContactExists: function (string, ip) {

    // Check.. I hate it. :D
    check (string, String);
    check (ip, String);

    // Validate IP & check for dupe
    if (RF.isValidIp(ip)) {
      RF.log('Checking for duplicate source IP', 1);
      if (rfContacts.find({source: ip}).count()) {
        RF.log('ERROR This source already exists in contact log', 2);
        return true;
      }
    } else {
      RF.log('ERROR Invalid IP when checking for duplicates', 2);
      return true;
    }

    // Validate string & check for dupe
    if (RF.isValidEmail(string)) {
      RF.log('Checking for duplicate email', 1);
      return (rfContacts.find({email: string}).count());
    } else if (RF.isValidTweeter(string)) {
      RF.log('Checking for duplicate twitter handle', 1);
      return (rfContacts.find({twitter: string}).count());
    } else {
      RF.log('ERROR Invalid contact when checking for duplicates', 2);
      return true;
    }

  },

});

