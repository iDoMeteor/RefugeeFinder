console.log('@Refugee_Finder by @iDoMeteor :: Loading API');

/*******************************************************************************
 * Refugee Finder API
 *
 * Pardon the dust, it will settle soon.
 *
 * ****************************************************************************/

RF = {

  /*****************
   * Check Google Analytics Configuration
   */

  gaEnabled: function() {
    return (
      idmGA && idmGA.pageview && idmGA.event
      && RF.getNestedConfig('google', 'enable')
      && RF.getNestedConfig('google', 'account')
    ) ? true : false;
  },


  // TODO: Doc these
  getConfig: function(key) {

    // Validate
    if (!RF.isString(key)) {
      RF.log('ERROR Attempting to get invalid string', 2);
      return '';
    }

    // Do it
    return (rf[key])
        ? rf[key]
        : false;

  },


  /**
   * getNestedConfig
   *
   * @name getNestedConfig
   * @function
   * @access public
   * @param {string} k1 Top level property, required
   * @param {string} k2 Second level property, optional
   * @param {string} k3 Tertiary property, optional
   * @return {any} Can return any valid data type
   */
  getNestedConfig: function(k1, k2, k3) {

    // Validate
    if (!RF.isString(k1)) {
      RF.log('ERROR Attempting to get invalid configuration value' + key.toString(), 2);
      return false
    }
    if (k2 && !RF.isString(k2)) {
      RF.log('ERROR Attempting to get invalid configuration value' + key.toString(), 2);
      return false
    }
    if (k3 && !RF.isString(k3)) {
      RF.log('ERROR Attempting to get invalid configuration value' + key.toString(), 2);
      return false
    }

    // Do it
    return (
            k3 && k2 && k1
               && rf[k1]
               && rf[k1][k2]
               && rf[k1][k2][k3]
            )
        ? rf[k1][k2][k3]
        : (
            k2 && k1
                && rf[k1]
                && rf[k1][k2]
            )
        ? rf[k1][k2]
        : (k1 && rf[k1])
            ? rf[k1]
            : (
                RF.log('ERROR Attempting to get invalid configuration value' + key.toString(),
                    2),
                false
            );  // How ya like them apples :>

  },


  /***************************************************************************
   *
   * @Summary         Check connection log for this an IP
   * @Method          ipExists
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      Returns true if checked IP exists in the iDM connection log
   *
   * ************************************************************************/

  ipExists: function(ip) {

    // Validate IP & check for dupe
    if (RF.isValidIp(ip)) {
      return (idmConnectionLog.find({source: ip}).count());
    } else {
      RF.log('ERROR Invalid IP when checking for duplicates');
      return false;
    }

  },


  /***************************************************************************
   *
   * @Summary         Checks contact log for current user's IP
   * @Method          ipIsInContacts
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Checks the contact log for the client IP
  ipIsInContacts: function(ip) {

    ip = ip || Meteor.userIp;
    RF.log('IP: ' + Meteor.userIp);
    // TODO: Search for IP in contacts

  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type Boolean
   * @Method          isBoolean
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of function
  isBoolean: function(value) {
    return ('boolean' == typeof (value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a string matching a Mongo ID
   * @Method          isCollectionId
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject to match a Meteor/Mongo collection _id string
  isCollectionId: function(value) {
    return (/^\x{24}$/.test(value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type date
   * @Method          isDate
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of string
  isDate: function(value) {
    return ('date' == typeof (value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type Function
   * @Method          isFunction
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of function
  isFunction: function(value) {
    return ('function' == typeof (value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a string of one or more digits
   * @Method          isNumber
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for [0-9]
  isNumber: function(value) {
    return (/^[0-9]?$/.test(value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type Object
   * @Method          isObject
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of object (could be array or something else)
  isObject: function(value) {
    return ('object' == typeof (value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type String
   * @Method          isString
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of string
  isString: function(value) {
    return ('string' == typeof (value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         XXX
   * @Method          isTemplate
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  isTemplate: function(value) {
    return (Blaze.isTemplate(value));
  },


  /***************************************************************************
   *
   * @Summary         XXX
   * @Method          isTemplateInstance
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  isTemplateInstance: function(value) {
    return value instanceof Blaze.TemplateInstance;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a valid email or Twitter address
   * @Method          isValidContact
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject (contact submission) to ensure it is either
  // a valid Twitter handle or email address (intranets supported)
  // and if so, returns true if the client's IP is not already
  // in the contact log or false otherwise.
  isValidContact: function(value) {

    // TODO: Check for dupes by ip and address

    // Validate type
    if (!RF.isString(value)) return;
    // Validate Twitter nick or email
    return (RF.isValidTweeter(value) || RF.isValidEmail(value))
        ? true : false;

  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a valid document for insertion
   * @Method          isValidContactDocument
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  isValidContactDocument: function(obj) {

    var whitelist = [
        'email',
        'label',
        'source',
        'stamp',
        'twitter',
    ]

    if (!RF.isObject(obj)) return false;
    if (_.omit(obj, whitelist).length) {
      return false;
    }

    // Label
    if (!RF.isString(obj.label)) return false;

    // Source
    if (!RF.isValidIp(obj.source)) return false;

    // Stamp
    if (!RF.isDate(obj.stamp)) return false;

    // Contact
    if (
        (RF.isValidEmail(obj.email))
        || (RF.isValidTweeter(obj.twitter))
    ) return true;

    return false;

  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a valid email, intranet friendly
   * @Method          isValidEmail
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject to make sure it is a valid email address that
  // can be parsed by a standard mail server, which is rather forgiving.
  // Allows for intranet addresses (ie; admin@server).
  isValidEmail: function(value) {
    if (!RF.isString(value)) return false;
    value = value.trim();
    return (/^.{1,255}\@[\w-.]{1,255}$/.test(value))
        ? true
        : false;
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter matches an IPv4 address
   * @Method          isValidIp
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  isValidIp: function(value) {
    if (!RF.isString(value)) return false;
    value = value.trim();
    return (/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value))
        ? true
        : false;
  },

  /***************************************************************************
   *
   * @Summary         Checks if parameter is a valid mail object
   * @Method          isValidMailObject
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  isValidMailObject: function(value) {

    // Validate
    if (!RF.isObject(value)) {
      RF.log('ERROR Attempting to validate improperly formatted mail'
              + ' object: \n'
              + JSON.stringify(value), 2);
      return false;
    }

    // Sanitize keys
    var whitelist = _.pick(value, 'to', 'from', 'subject', 'text');

    // Check for additions
    if (whitelist.keys().length != value.keys().length) {
      RF.log('ERROR Attempting to validate potentially malformed'
              + ' notification object: \n'
              + JSON.stringify(value), 2);
      return false;
    }

    // Lazyyyy
    return (
        (RF.isValidEmail(whitelist.to))
        && (RF.isValidEmail(whitelist.from))
        && (RF.isValidString(whitelist.subject))
        && (RF.isValidString(whitelist.text))
    ) ? (
        true
    ) : (
        RF.log('ERROR Validating mail object failed'),
        false
    )

  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is of type String
   * @Method          isValidString
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tests a subject for type of string
  isValidString: function(value) {
    // Alias
    return RF.isString(value);
  },


  /***************************************************************************
   *
   * @Summary         Checks if parameter is a valid Twitter handle
   * @Method          isValidTweeter
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Tweet tweet tweetildeedildeet, is it valid?
  isValidTweeter: function(value) {
    if (!RF.isString(value)) return false;
    value = value.trim();
    return (/^@\w{1,15}$/.test(value))
        ? true
        : false;
  },


  /*****************
   *
   */

  kadiraEnabled: function() {
    return (
        RF.getNestedConfig('kadira', 'enable')
        && RF.getNestedConfig('kadira', 'secret')
        && RF.getNestedConfig('kadira', 'key')
    )
    ? true : false;
  },


  /***************************************************************************
   *
   * @Summary         Logger of console messages & alerts
   * @Method          log
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *            Options (ordered here in relative priority for use)
   *                  message
   *                  type
   *                      * No type will ever alter notifyUser or data
   *                      critical
   *                      danger
   *                      error
   *                      failure
   *                      info
   *                      security
   *                      success
   *                      warning
   *                  sendEvent
   *                  notifyUser
   *                  notifyAdmin
   *                  data
   *
   *  TODO:
   *          Not all messages need to be stringified,
   *          Fix when putting in data object check
   *
   * ************************************************************************/

  log: function(options) {

    var chop        = 'info'; // Default type
    var wood        = {};
    var log         = {};
    // Process message for console & set BS alert context
    var logJam = {

      critical: function(obj) {
        console.log('RF CRITICAL ERROR: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = 'danger';
        obj.notifyAdmin = true;
        obj.sendEvent   = true;
        return obj;
      },

      danger: function(obj) {
        console.log('RF DANGER: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = 'danger';
        obj.sendEvent   = true;
        return obj;
      },

      debug: function(obj) {
        if (RF.getConfig('debug')) {
          console.log('RF DEBUG: '
                      + JSON.stringify(obj, null, 4)
                     );
        }
        obj.notifyAdmin = false;
        obj.notifyUser  = false;
        obj.sendEvent   = false;
        return obj;
      },

      deployed: function(obj) {
        console.log('RF Deployed: '
                    + JSON.stringify(obj, null, 4)
                   );
        // TODO: Send rollbar deployment
        obj.alertType   = 'deployment';
        obj.sendEvent   = true;
        return obj;
      },

      error: function(obj) {
        console.log('RF ERROR: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = 'danger';
        obj.notifyAdmin = true;
        obj.sendEvent   = true;
        return obj;
      },

      failure: function(obj) {
        console.log('RF FAILURE: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = 'danger';
        obj.notifyAdmin = true;
        obj.sendEvent   = true;
        return obj;
      },

      info: function(obj) {
        console.log('RF INFO: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = obj.type;
        return obj;
      },

      security: function(obj) {
        console.log('RF SECURITY ISSUE: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = 'danger';
        obj.notifyAdmin = true;
        obj.sendEvent   = true;
        return obj;
      },

      success: function(obj) {
        console.log('RF SUCCESS: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = obj.type;
        return obj;
      },

      warning: function(obj) {
        console.log('RF WARNING: '
                    + JSON.stringify(obj, null, 4)
                   );
        obj.alertType   = obj.type;
        return obj;
      },

    };
    var useAstro    = (
      RF.getNestedConfig('astronomer', 'enable')
      && RF.getNestedConfig('astronomer', 'sendLogEvents')
    ) ? true : false;
    var useGAEvent    = (
      RF.getNestedConfig('google', 'enable')
      && RF.getNestedConfig('google', 'sendLogEvents')
    ) ? true : false;
    var useGAEvent    = (
      RF.getNestedConfig('rollbar', 'enable')
      && RF.getNestedConfig('rollbar', 'sendLogEvents')
    ) ? true : false;
    var userId      = 'Anonymous User'; // TODO: Pass in user ID
    var type        = null;

    if (RF.isObject(options)) {

      // Validate
      wood.message        = (RF.isString(options.message))
                          ? options.message : 'Invalid message';
      wood.data           = (RF.isString(options.data) || RF.isObject(options.data))
                          ? options.data : null;
      wood.date           = new Date();
      wood.notifyAdmin    = (RF.isBoolean(options.notifyAdmin))
                          ? options.notifyAdmin : false;
      wood.notifyUser     = (RF.isBoolean(options.notifyUser))
                          ? options.notifyUser : false;
      wood.sendEvent      = (RF.isBoolean(options.sendEvent))
                          ? options.sendEvent : false;
      wood.type           = (
                              (type = options.type)
                              && ('function' == typeof (logJam[type]))
                          ) ? type : 'info';
      chop                = wood.type;

    } else if (RF.isString(options)) {
      // Deprecated form but we'll take it
      wood.message = options;
      wood.type    = 'info';
    } else {
      console.log('#RF ERROR Invalid call to logger');
      return false;
    }

    // BzzzzZZZzzzzzZZZZzzz
    log = logJam[chop](wood);

    // Log to database
    Log.insert(log);

    // Notify admin if requested
    if (log.notifyAdmin) {
      RF.notifyAdmin(JSON.stringify(log, null, 4));
    }

    // Send events
    if (log.sendEvent) {

      // Astronomer
      // if (useAstro)
      // Google Analytics
      if (useGAEvent && RF.gaEnabled) idmGA.event('Log', log.type, null, null, log);
      // Rollbar
      // if (useRollbar)


    }

    // Notify user if desired
    if (log.notifyUser) {
      RF.popAlert(log.message, log.alertType);
    }

    return;

  },


  matchProfileSave: function(modal) {
  },

  /***************************************************************************
   *
   * @Summary         XXX
   * @Method          notifyAdmin
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  // Get total row count
  notifyAdmin: function(message, callback) {

    if (Meteor.isServer) {
      this.unblock();
    } else {
      return (RF.isFunction(callback)) ? callback(false) : null;
    }

    // Locals
    var to      = (rf && rf.contact && RF.isString(rf.contact.recips))
                ? rf.contact.recips
                : Meteor.users.findOne({}, {fields: {email: 1}}).email;
    var from    = to;
    var subject = rf.title || 'Contact Request from Your RF Site!';
    var mail    = {};
    var message = (RF.isString(message)) ? message : (
        RF.log('EROR Attempting to notify admin with invalid message: '
                + JSON.stringify(message, null, 4), 2),
        'Invalid message, check logs @' + new Date()
    )

    // Validate
    if (!RF.isValidEmail(to)) {
      RF.log('ERROR Admin contact has been improperly configured');
      return (RF.isFunction(callback)) ? callback(false) : null;
    }

    // Formulate mail object
    mail = {
      from: from,
      subject: subject,
      text: message,
      to: to,
    }

    // Log it
    RF.logAdminNotification(mail);

    // Send it
    Email.send(mail);

    return (RF.isFunction(callback)) ? callback(true) : null;
  },


  /***************************************************************************
   *
   * @Summary         XXX
   * @Method          popAlert
   * @Param           n/a
   * @Returns         undefined
   * @Location        Client, Server
   *
   * @Description
   *
   *      XXX
   *
   * ************************************************************************/

  popAlert: function(content, type, duration) {

    // Validate
    if (!RF.isString(content)) {
      RF.log('ERROR Invalid attempt to create alert');
      return false;
    }
    if (!RF.isString(type)) {
      type = rf.bootstrap.alert.type;
    }
    if (!RF.isNumber(duration)) {
      duration = rf.bootstrap.alert.duration;
    }
    var data = {
      content: content,
      duration: duration,
      type: type,
    }

    // Render editor into DOM w/data
    Blaze.renderWithData(Template.rfAlert, data, $('body')[0]);

  },


  popModal: function(modal) {
  },

  sendContact: function(modal) {
  },

}
