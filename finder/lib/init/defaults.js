console.log('@Refugee_Finder by @iDoMeteor :: Loading Configuration');

/*******************************************************************************
 * Refugee Finder Configuration
 *
 * A couple lingering things here that will may be removed soon.
 * ****************************************************************************/

rf = {

  // CORS Access Control Origin
  // Serialized origin list
  acao: 'localhost:*, https://ddp*.meteor.com, *.refugeefinder.org',

  // Astronomer
  astronomer: {
    key:            '',
    enable:         false,
    secret:         '',
    sendLogEvents:  true,
  },

  // Bootstrap config
  bootstrap: {

    alert: {
      // Defaults, can override when called
      closeLabel: 'Close',
      duration: 7000,
      type: 'info',
    },
    carouselInterval: 6000,

  },

  // Contact form config
  contact: {

    btnString: 'Confirm Request!', // TOOD: Allow this to be passed in short tag
    btnStringDetailed: 'Send!',
    detailedFormInvalidSubmission: 'Your submission requires more information, please try again.',
    from: 'idometeor@gmail.com',
    lightboxLogTitle: 'Contact Log',
    lightboxTitle: 'Direct Communication Form',
    recips: 'idometeor@gmail.com',
    showDetailed: true,
    showDetailedIcon: 'fa-pencil',
    showDetailedText: 'Write Us',
    thankYouAlert: 'We have received your request and will respond at our earliest opportunity!',
    thankYouText: 'Thank you! We won\'t abuse your trust! :>',

  },

  // Verbose console messages
  debug: true,

  // TODO: Replace above w/this
  log: {
    clientSideConsoleLogs: true,
    debug: true,
    outputTypes: [
        'critical',
        'danger',
        'error',
        'failure',
        'info',
        'security',
        'warning',
    ], // Only log types listed here will be logged to console
  },

  // Google Analytics
  google: {
    account: '',
    cookie: {
      // If you want to customize your cookie,
      // disable auto & set the rest
      auto: true,
      domain: null,
      expires: null,
      name: null,
    },
    debug: true,
    debugTrace: false, // Very verbose
    enable: true,
    sendLogEvents: true,
    trackInPage: true,
    trackInterests: true,
  },

  // Images
  logoAbs: 'http://refugeefinder.org/logo.png',
  logoRel: '/logo.png',
  logoNavigationAbs: 'http://refugeefinder.org/rf/images/logo-24.png',
  logoNavigationRel: '/rf/images/logo-24.png',

  // Kadira Keys
  kadira: {
    key: 'xxx',
    enable: false,
    secret: 'xxx',
  },

  // Whether or not to allow an anonymous user to run the linter(s)
  linter: {
    requireLogin: false,
  },

  // Meta Tags
  meta: {

    'http-equiv': {
      'content-type': 'text/html; charset=utf-8',
      'content-script-type': 'text/javascript',
      'content-lanuage': 'EN',
      // Above should always be first
      author: '@iDoMeteor - Marquette, MI',
      copyright: 'The author retains all copyrights to all code, script or graphics which are directly related to the @Refugee_Finder app.  @iDoMeteor retains all copyrights to any intellectual property, logos, or graphics created exclusively for @Refugee_Finder. Copyright 2015',
      description: '@Refugee_Finder is a web app to help refugees reconnect with friends and loved ones and find help centers & border crossings.',
      distribution: 'Global',
      expires: new Date().getTime() + 86400000, // One week from now
      keywords: 'meteor, javascript, cms, content management, content management system, open source',
      'last-modified': new Date(), // TODO: This should come from DB stamps really
      'revisit-after': '7 Days',
      robots: 'index,follow',
      'X-UA-Compatible': 'IE=edge',
    },

    charset: 'UTF-8', // Has to come after above

    'itemprop': {
      description: '@Refugee_Finder is a web app to help refugees reconnect with friends and loved ones and find help centers & border crossings.',
      image: 'http://refugeefinder.org/logo.png',
      title: '@Refugee_Finder by @iDoMeteor',
    },

    link: {
      favicon: '/favicon.png',
    },

    name: {

      dc: { title: '@Refugee_Finder by @iDoMeteor' },

      geo: {
        placename: 'Marquette',
        position: '46.5;-87.4',
        region: 'US-MI',
      },

      // Would you like to play a game..
      icbm: '46.5, -87.4',

      // Twitter Card tags
      twitter: {
        creator: '@iDoMeteor',
        description: '@Refugee_Finder is a web app to help refugees reconnect with friends and loved ones and find help centers & border crossings.',
        image: 'http://refugeefinder.org/logo.png',
        site: 'http://refugeefinder.org',
        title: '@Refugee_Finder by @iDoMeteor',
      },

      viewport: 'width=device-width, initial-scale=1',
      // viewport: 'width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no',

    },

    property: {

      // Open Graph tags
      og: {
        description: '@Refugee_Finder is a web app to help refugees reconnect with friends and loved ones and find help centers & border crossings.',
        image: 'http://refugeefinder.org/logo.png',
        site_name: '@Refugee_Finder by @iDoMeteor',
        title: '@Refugee_Finder by @iDoMeteor',
        type: 'site',
        url: 'http://refugeefinder.org',
      },

    },

    title: '@Refugee_Finder by @iDoMeteor',

  },

  // Numerics
  numerics: {

    // Maximum publication limit, eventually will need a pager
    publishLimit: 100,

  },

  // Prefix
  prefix: 'rf',

  // Rollbar
  rollbar: {

    clientToken:    '',
    enable:         false,
    sendLogEvents:  false,
    serverToken:    '',

  },

  // Site Title
  title: '@Refugee_Finder by @iDoMeteor',

  // Strings
  strings: {
    adminName: 'Administrator',
    adminUsername: 'admin',
    authenticationButtonCreateAccount: 'Create Admin User',
    authenticationButtonLogin: 'Authenticate',
    authenticationCancelled: 'Authentication cancelled',
    authenticationHeading: 'Authenticate Yourself',
    authenticationLoginFailure: 'Authentication failed.',
    authenticationLighboxTitle: 'Login',
    authenticationLoginSuccess: 'Git\'er done!',
    authenticationLogoutFailure: 'You could not be logged out.',
    authenticationLogoutSuccess: 'Thank you, drive thru.',
    authenticationTagline: 'Present your credentials for inspection.',
    cancelLabel: 'Cancel',
    emailLabel: 'Email Address',
    loadingText: 'Loading..',
    passwordLabel: 'Password',
    siteStatsLabel: 'Site Statistics',
    submitLabel: 'Submit',
    userIpLabel: 'Your IP',

    newUserWelcome: 'Thanks for installing @Refugee_Finder by @iDoMeteor!',
  },

  // Toggles
  toggles: {
    notifyAdminOnLoginSuccess: true,
    notifyAdminOnLoginFailure: true,
  },

}
